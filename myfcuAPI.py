import requests

#   Get student absent record
def GetAbsRecord(stuid, sessionId):
    cookie = {}
    cookie['ASP.NET_SessionId'] = sessionId
    link = 'https://myfcu.fcu.edu.tw/main/S3401/s3401_leave.aspx/GetStuabscHis'
    r = requests.post(link, json={'stuid': stuid}, cookies=cookie)
    if r.status_code != 200:
        print('status code: ', r.status_code)
        raise RuntimeError('Cannot get data from server')
    data = r.json()['d']
    if len(data) == 0:
        return None
    return data

#   Get student's basic information
def GetUserInfo(stuid, sessionId):
    link = 'https://myfcu.fcu.edu.tw/main/webClientMyFcuMain.aspx/GetUserInfo'
    cookie = {}
    cookie['ASP.NET_SessionId'] = sessionId
    r = requests.post(link, json={'str': stuid}, cookies=cookie)
    if r.status_code != 200:
        print('status code: ', r.status_code)
        raise RuntimeError('Cannot get data from server')
    data = r.json()['d']
    # if target student doesn't exist
    if data['uid'] == 'MYFCU':
        return None
    return data

    