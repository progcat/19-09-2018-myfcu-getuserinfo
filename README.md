# 19-09-2018-MyFCU-GetUserInfo
A potential leak of personal data found in myfcu.fcu.edu.tw with GetUserInfo request.
```
This vulnerability was found at 19-9-2018, 
and fixed in 3 days(not sure) after reporting.
```

Thanks for [逢甲大學黑客社 (HackerSir)](http://hackersir.org/2015/) for helping me report this vulnerability.

## Files
```myfcuAPI.py``` - The implementation of this vulnerability and the implementation of getting student absent record exploit shown in "freshman tea party"(新生茶會).
```test_code.py``` - A neutralized crawler

## Details
```MyFCU``` - A web application which is commonly used in Feng Chia University.

This vulnerability allows attacker to get user infomation from ```MyFCU``` without login to it.

```GetUserInfo``` is the API call to get user information including: name, sex, NID(Feng Chia Network ID), class, department, personal email, which should be unable to get to others hand without permission.
The call is fairly simple to use, all you need is a active ```ASP.NET_SessionId``` in cookie and student Id you targeted. If the request returned with status code 500, that problably mean you got the wrong ```ASP.NET_SessionId```.


Once you get status code 200, it should return a array```'d':{}``` with wanted data if the target student Id is valid.

**Here's the weird things:**

At the first time I found this vulnerability, if the student Id I sent is invalid, it return a array```'d':{}``` with nothing in it. 

But after few hours I discovered it, request with the invalid student Id, it returns the array```'d':{}``` with the structure of student info. although most of the fields are empty or fill with junk like: ```"MYFCU"``` string in the ```uid``` field. 
Looks like they found out someone are try to "play" with their site, and instead of take a look to find out how the attacker do it, they decided to just return some junky data. 


